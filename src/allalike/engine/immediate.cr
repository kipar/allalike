require "crsfml"
require "../utils/pool"
require "./assets"

module Engine
  class Immediate
    property! font : SF::Font?
    property text_size = 12
    property text_color : SF::Color = SF::Color::White
    property text_style : SF::Text::Style = SF::Text::Regular

    getter! tile_base : SF::Texture
    property tile_size = 10
    @tile_base_width = 10

    @labels = LinearPool(SF::Text).new
    @sprites = LinearPool(SF::Sprite).new
    @tiled_sprites = LinearPool(SF::Sprite).new
    @rectangles = LinearPool(SF::RectangleShape).new
    @circles = LinearPool(SF::CircleShape).new

    def stats
      "#{@labels.size} #{@sprites.size} #{@tiled_sprites.size} #{@rectangles.size} #{@circles.size}"
    end

    def reset
      @labels.reset
      @sprites.reset
      @tiled_sprites.reset
      @rectangles.reset
      @circles.reset
    end

    def initialize(@owner : SF::RenderWindow)
    end

    def text(x, y, string, *, size = text_size, color = text_color, font = self.font, style = text_style, centered = false)
      label = @labels.get
      label.font = font unless label.font == font
      label.character_size = size unless label.character_size == size
      label.color = color unless label.color == color
      label.style = style unless label.style == style
      label.string = string unless label.string == string
      states =
        if centered
          width = label.local_bounds.width
          SF::RenderStates.new(SF::Transform.new.translate(x - width/2, y))
        else
          SF::RenderStates.new(SF::Transform.new.translate(x, y))
        end
      label.draw(@owner, states)
    end

    def sprite(x, y, tex, *, centered = false, width = 1.0, height = 1.0, rotate = 0.0)
      trf = SF::Transform.new.translate(x, y).rotate(rotate/Math::PI*180).scale(width, height)
      trf = trf.translate(-tex.size/2) if centered
      states = SF::RenderStates.new(trf)
      spr = @sprites.get
      unless spr.texture == tex
        spr.set_texture tex, true
      end
      spr.draw(@owner, states)
    end

    def tile_base=(tex : SF::Texture)
      @tile_base = tex
      @tile_base_width = tex.size.x
      @tiled_sprites.raw.each do |spr|
        spr.texture = tex
      end
    end

    def tile(x, y, tileid, *, centered = false, width = 1.0, height = 1.0, rotate = 0.0)
      trf = SF::Transform.new.translate(x, y).rotate(rotate/Math::PI*180).scale(width, height)
      trf = trf.translate(-vec(@tile_size, @tile_size)/2) if centered
      states = SF::RenderStates.new(trf)
      spr = @tiled_sprites.get { SF::Sprite.new.tap { |sprite| sprite.texture = tile_base } }
      nx = {@tile_base_width / tile_size, 1}.max
      ax = (tileid.to_i % nx)*@tile_size
      ay = (tileid.to_i / nx)*@tile_size
      spr.texture_rect = SF::IntRect.new(ax, ay, @tile_size, @tile_size)
      spr.draw(@owner, states)
    end

    def rect(x, y, width, height, color)
      trf = SF::Transform.new.translate(x, y).scale(width, height)
      states = SF::RenderStates.new(trf)
      rect = @rectangles.get { SF::RectangleShape.new.tap { |rect| rect.size = vec(1, 1) } }
      rect.fill_color = color
      rect.draw(@owner, states)
    end

    def circle(x, y, radius, color)
      trf = SF::Transform.new.translate(x - radius, y - radius).scale(radius, radius)
      states = SF::RenderStates.new(trf)
      circle = @circles.get { SF::CircleShape.new(1) }
      circle.fill_color = color
      circle.draw(@owner, states)
    end

    def line(x1, y1, x2, y2, color, width = 1.0)
      angle = Math.atan2(y2 - y1, x2 - x1)
      length = Math.sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2))
      line(x1, y1, angle: angle, length: length, color: color, width: width)
    end

    def line(x, y, *, angle, length, color, width = 1.0)
      trf = SF::Transform.new.translate(x, y).rotate(angle/Math::PI*180).scale(length, width).translate(0, -0.5)
      states = SF::RenderStates.new(trf)
      rect = @rectangles.get { SF::RectangleShape.new.tap { |rect| rect.size = vec(1, 1) } }
      rect.fill_color = color
      rect.draw(@owner, states)
    end
  end
end
