require "crsfml"
require "../utils/counter"
require "../config"
require "./immediate"

# TODO - ugly
alias MyVec = SF::Vector2(Float32)

def vec(x, y)
  MyVec.new(Float32.new(x), Float32.new(y))
end

module Engine
  abstract class App
    getter window : SF::RenderWindow
    getter immediate : Immediate

    # TODO - make it a nice macro
    def on_closing : Bool
      true
    end

    def on_key_pressed(key); end

    def on_key_released(key); end

    def on_mouse_pressed(button, x, y); end

    def on_mouse_released(button, x, y); end

    def on_mouse_moved(x, y); end

    def on_text_entered(text); end

    abstract def draw
    abstract def process

    def initialize
      @window = SF::RenderWindow.new(SF::VideoMode.new(800, 600), "My window")
      @window.vertical_sync_enabled = true
      # @window.framerate_limit = 60
      @quitting = false
      @phys_timer = SF::Clock.new
      @fps = EventCounter.new
      @ups = EventCounter.new
      @elapsed = 0f32
      @immediate = Immediate.new(@window)
      Assets.load("./res")
    end

    private def do_draw
      @fps.inc
      draw
    end

    private def do_process
      @ups.inc
      process
    end

    def run
      @phys_timer.restart
      while !@quitting
        # check all the window's events that were triggered since the last iteration of the loop
        while event = @window.poll_event
          # TODO - make it a nice macro
          case event
          when SF::Event::Closed
            return unless on_closing
            @quitting = true
          when SF::Event::KeyPressed
            on_key_pressed(event.code)
          when SF::Event::KeyReleased
            on_key_released(event.code)
          when SF::Event::MouseButtonPressed
            on_mouse_pressed(event.button, event.x, event.y)
          when SF::Event::MouseButtonReleased
            on_mouse_released(event.button, event.x, event.y)
          when SF::Event::MouseMoved
            on_mouse_moved(event.x, event.y)
          when SF::Event::TextEntered
            on_text_entered(event.unicode.chr)
          end
        end
        @elapsed += @phys_timer.restart.as_seconds
        while @elapsed > PHYS_DT
          do_process
          @elapsed -= PHYS_DT
        end
        @window.clear
        @immediate.reset
        do_draw
        @window.display
      end
      @window.close
    end
  end
end
