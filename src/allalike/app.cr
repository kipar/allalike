require "crsfml"
require "./engine/*"
require "./inventory"
require "./game/world"

module Allalike
  class GameApp < Engine::App
    getter world

    def map
      player.map
    end

    def player
      @world.player
    end

    def weapon
      @world.player.weapon
    end

    def on_mouse_released(button, x, y)
      if @inventory.active
        action = case button
                 when .left?
                   InvAction::Use
                 when .right?
                   InvAction::Drop
                 else
                   InvAction::Check
                 end
        @inventory.hide unless @inventory.item_action(x, y, action)
      elsif button.right?
        player.reload
      end
    end

    def do_shot(x, y)
      dx = x - Engine::SCREENX/2
      dy = y - Engine::SCREENY/2
      angle = Math.atan2(dy, dx)
      if player.ammo == 0
        player.reload
      else
        player.shoot angle
      end
    end

    def on_mouse_pressed(button, x, y)
      return if @inventory.active
      if button.left?
        do_shot(x, y)
      end
    end

    def on_key_pressed(key)
      if key == SF::Keyboard::Key::E
        player.open_door
      end
    end

    def on_key_released(key)
      if key == SF::Keyboard::Key::I
        @inventory.active ? @inventory.hide : @inventory.show
      end
      if key == SF::Keyboard::Key::R
        player.reload
      end
    end

    def draw_background(plx, ply)
      Game::MAP_SIZE.times do |x|
        Game::MAP_SIZE.times do |y|
          scrx = x*Game::MAP_TILE_SIZE - plx + Engine::SCREENX/2
          next unless (-Game::MAP_TILE_SIZE..Engine::SCREENX).includes? scrx
          scry = y*Game::MAP_TILE_SIZE - ply + Engine::SCREENY/2
          next unless (-Game::MAP_TILE_SIZE..Engine::SCREENY).includes? scry
          @immediate.tile(scrx, scry, map.map_data[y*Game::MAP_SIZE + x][:picture],
            width: 1.0*Game::MAP_TILE_SIZE/@immediate.tile_size,
            height: 1.0*Game::MAP_TILE_SIZE/@immediate.tile_size,
          )
        end
      end
    end

    def draw_hud
      @immediate.text(0, Engine::SCREENY - 100, "Ammo=#{player.ammo} / #{player.weapon.maxammo}", size: 50)
    end

    def draw_own_status(ax, ay)
      if player.delay > 0
        @immediate.rect 20, Engine::SCREENY - 50, 550*player.delay/{1, player.maxdelay}.max, 5, SF::Color::Yellow
      end
      pos = SF::Mouse.get_position(@window)
      dx = pos.x - Engine::SCREENX/2
      dy = pos.y - Engine::SCREENY/2
      # TODO - better method
      angle = Math.atan2(dy, dx)
      size = 0.07 * {player.scatter / 10, 10}.max
      @immediate.sprite(ax + dx, ay + dy,
        Engine::Tex["sword.png"], centered: true, width: size, height: size)
    end

    @ticks = 0

    def draw
      draw_background(player.x, player.y)
      #
      offset = vec(-player.x + Engine::SCREENX/2, -player.y + Engine::SCREENY/2)
      @ticks += 1
      map.objects.select(&.is_a?(Game::LootItem)).each do |obj|
        next if (obj.pos - player.pos).lengthsq > Engine::SCREENX*Engine::SCREENX
        ax = obj.x + offset.x
        ay = obj.y + offset.y
        tex = obj.picture_id
        angle = 1.0*@ticks/60
        @immediate.tile(ax, ay, tex, centered: true, width: 0.7, height: 0.7, rotate: angle)
      end
      map.objects.reject(&.is_a?(Game::LootItem)).each do |obj|
        next if (obj.pos - player.pos).lengthsq > Engine::SCREENX*Engine::SCREENX
        ax = obj.x + offset.x
        ay = obj.y + offset.y
        case obj
        when Game::Player
          @immediate.tile(ax, ay, obj.picture_id, centered: true, width: 0.7, height: 0.7)
          @immediate.rect ax - 25, ay - 30, 50, 5, SF::Color::Red
          @immediate.rect ax - 25, ay - 30, 50*player.hp/100, 5, SF::Color::Green
        when Game::Monster
          @immediate.tile(ax, ay, obj.picture_id, centered: true, width: 1.0, height: 1.0)
        when Game::Bullet
          angle = Math.atan2(obj.vy, obj.vx)
          # @immediate.line(ax, ay, angle: angle, length: 20, color: SF::Color::Yellow)
          @immediate.tile(ax, ay, obj.picture_id, centered: true,
            width: 0.7, height: 0.7,
            rotate: angle + Math::PI/2)
        end
      end
      draw_own_status player.x, player.y
      draw_hud
      # @immediate.text(0, 40, @immediate.stats)

      @inventory.draw(@immediate)
      status = "FPS=#{@fps.value}, UPS=#{@ups.value}"
      # status = @immediate.stats
      @immediate.text(0, 0, status, style: (SF::Text::Bold | SF::Text::Underlined))
    end

    DIR_KEYS = {
      SF::Keyboard::Key::W     => {0, -1},
      SF::Keyboard::Key::S     => {0, 1},
      SF::Keyboard::Key::A     => {-1, 0},
      SF::Keyboard::Key::D     => {1, 0},
      SF::Keyboard::Key::Up    => {0, -1},
      SF::Keyboard::Key::Down  => {0, 1},
      SF::Keyboard::Key::Left  => {-1, 0},
      SF::Keyboard::Key::Right => {1, 0},
    }

    def process_player
      pos = SF::Mouse.get_position(@window)
      dx = pos.x - Engine::SCREENX/2
      dy = pos.y - Engine::SCREENY/2
      player.angle = Math.atan2(dy, dx)

      step = [0, 0]
      DIR_KEYS.each do |k, v|
        if SF::Keyboard.key_pressed? k
          step[0] += v[0]
          step[1] += v[1]
        end
      end
      player.target = CP.v(100*step[0], 100*step[1])
      if SF::Mouse.button_pressed?(SF::Mouse::Left) && player.weapon.autofire
        do_shot pos.x, pos.y
      end
      if SF::Mouse.button_pressed?(SF::Mouse::Right) && player.weapon.singlereload
        player.reload
      end
    end

    def process
      unless @inventory.active
        process_player
        @world.process
      end
      Fiber.yield
    end

    def initialize
      super()

      @immediate.font = Engine::Font["default.ttf"] # font is a SF::Font
      @immediate.text_size = 16                     # in pixels, not points!
      @immediate.text_color = SF::Color::Green

      @immediate.tile_base = Engine::Tex["tiles.png"]
      @immediate.tile_size = 64

      @inventory = Inventory.new(0.25, 0.25, 0.5, 0.5)

      @world = Game::World.new
    end
  end
end
