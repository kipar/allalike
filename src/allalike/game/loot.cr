require "chipmunk"
require "./general"
require "./thing"

module Game
  class LootItem < GameObject
    set_material(Shot)
    set_bodytype(Bullet)

    def initialize(amap, @thing : Thing, ax, ay)
      super(amap, ax, ay)
    end

    def picture_id
      @thing.picture_id
    end

    def collide_player(player)
      @thing.taken player
      @thing.wear
      die
      false
    end

    def collide_monster(monster)
      # monster.taken thing
      # die
      return false
    end

    def collide_bullet(bullet)
      return false
    end
  end
end
