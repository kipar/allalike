require "chipmunk"
require "../app"
require "./general"
require "./walls"
require "./physics_pool"
require "../bearlib/mapgen"

module Game
  enum TT
    FLOOR
    WALL
    WEST
    SOUTH
    NORTH
    EAST
    NW
    NE
    SW
    SE
    CORN_NW
    CORN_NE
    CORN_SW
    CORN_SE

    DOOR_N,
    DOOR_S,
    DOOR_W,
    DOOR_E,
    OPEN_DOOR_N,
    OPEN_DOOR_S,
    OPEN_DOOR_W,
    OPEN_DOOR_E,
    NO_DOOR_N,
    NO_DOOR_S,
    NO_DOOR_W,
    NO_DOOR_E,
  end

  enum LogicalTile
    Wall
    Room
    Passage
    VertDoor
    HorizDoor
    UpStair
    DownStair
    Chest

    def door?
      {VertDoor, HorizDoor}.includes? self
    end
  end

  BACK_TILES = [22*128 + 48, 25*128 + 76,                           # floor, wall
                23*128 + 55, 23*128 + 63, 23*128 + 71, 23*128 + 79, # side
                24*128 + 60, 24*128 + 61, 25*128 + 60, 25*128 + 61, # corner
                25*128 + 39, 25*128 + 38, 24*128 + 39, 24*128 + 38, #  inv corner
                24*128 + 14, 25*128 + 14, 25*128 + 11, 25*128 + 12, # doors
                24*128 + 13, 25*128 + 13, 24*128 + 11, 24*128 + 12, # open_doors
                23*128 + 70, 23*128 + 62, 23*128 + 54, 23*128 + 78, # no_doors
                # 23*128 + 20, 23*128 + 21,                           # alt floor
                # 24*128 + 31, 24*128 + 32, 25*128 + 31, 25*128 + 32, # alt corner
                # 23*128 + 49, 23*128 + 57, 23*128 + 65, 23*128 + 73  # alt side
                0]
  OPEN_DOORS = {
    BACK_TILES[TT::DOOR_N.to_i] => BACK_TILES[TT::OPEN_DOOR_N.to_i],
    BACK_TILES[TT::DOOR_S.to_i] => BACK_TILES[TT::OPEN_DOOR_S.to_i],
    BACK_TILES[TT::DOOR_W.to_i] => BACK_TILES[TT::OPEN_DOOR_W.to_i],
    BACK_TILES[TT::DOOR_E.to_i] => BACK_TILES[TT::OPEN_DOOR_E.to_i],
  }
  NO_DOORS = {
    BACK_TILES[TT::DOOR_N.to_i] => BACK_TILES[TT::NO_DOOR_N.to_i],
    BACK_TILES[TT::DOOR_S.to_i] => BACK_TILES[TT::NO_DOOR_S.to_i],
    BACK_TILES[TT::DOOR_W.to_i] => BACK_TILES[TT::NO_DOOR_W.to_i],
    BACK_TILES[TT::DOOR_E.to_i] => BACK_TILES[TT::NO_DOOR_E.to_i],
  }

  abstract class MapGenerator
  end

  class BearMapGenerator < MapGenerator
    @tile_data = Array(BearLibMG::CellType).new(0)
    @size : Int32

    def initialize(*, @double : Bool, @typ : BearLibMG::MapGenerator)
      @size = @double ? MAP_SIZE/2 : MAP_SIZE
    end

    private def double_size
      new_tile_data = Array(BearLibMG::CellType).new(@size*@size*4) do |i|
        x = i % (@size*2)
        y = i / (@size*2)
        @tile_data[y/2*@size + x/2]
      end
      @tile_data = new_tile_data
      @size *= 2
    end

    private def get_tile(north, south, west, east, nw, ne, sw, se)
      return TT::NW if north && west
      return TT::NE if north && east
      return TT::SW if south && west
      return TT::SE if south && east
      return TT::NORTH if north
      return TT::SOUTH if south
      return TT::WEST if west
      return TT::EAST if east
      return TT::CORN_NW if nw
      return TT::CORN_NE if ne
      return TT::CORN_SW if sw
      return TT::CORN_SE if se
      return TT::FLOOR
    end
    private def get_door(north, south, west, east)
      return TT::DOOR_N if north
      return TT::DOOR_S if south
      return TT::DOOR_W if west
      return TT::DOOR_E if east
      return TT::FLOOR
    end

    def get_logical(cur, tile)
      return LogicalTile::Wall if tile == TT::WALL
      return LogicalTile::VertDoor if {TT::DOOR_N, TT::DOOR_S}.includes? tile
      return LogicalTile::HorizDoor if {TT::DOOR_W, TT::DOOR_E}.includes? tile
      return LogicalTile::Room if cur == BearLibMG::CellType::TILE_BIGROOM
      return LogicalTile::Passage
    end

    def generate(data)
      @tile_data = BearMG.generate_map(@size, @typ, 0)
      double_size if @double
      (@size*@size).times do |i|
        x = i % MAP_SIZE
        y = i / MAP_SIZE
        cur = @tile_data[y*MAP_SIZE + x]
        if !cur.passable? || x == 0 || x == MAP_SIZE - 1 || y == 0 || y == MAP_SIZE - 1
          tile = TT::WALL
        elsif cur == BearLibMG::CellType::TILE_DOOR
          north = !@tile_data[x + (y - 1)*MAP_SIZE].passable?
          east = !@tile_data[(x + 1) + y*MAP_SIZE].passable?
          west = !@tile_data[(x - 1) + y*MAP_SIZE].passable?
          south = !@tile_data[x + (y + 1)*MAP_SIZE].passable?
          tile = get_door(north, south, west, east)
        else
          north = !@tile_data[x + (y - 1)*MAP_SIZE].passable?
          east = !@tile_data[(x + 1) + y*MAP_SIZE].passable?
          west = !@tile_data[(x - 1) + y*MAP_SIZE].passable?
          south = !@tile_data[x + (y + 1)*MAP_SIZE].passable?
          nw = !@tile_data[(x - 1) + (y - 1)*MAP_SIZE].passable?
          ne = !@tile_data[(x + 1) + (y - 1)*MAP_SIZE].passable?
          sw = !@tile_data[(x - 1) + (y + 1)*MAP_SIZE].passable?
          se = !@tile_data[(x + 1) + (y + 1)*MAP_SIZE].passable?
          tile = get_tile(north, south, west, east, nw, ne, sw, se)
        end
        data[i] = {
          pass:    cur.passable? && cur != BearLibMG::CellType::TILE_DOOR,
          picture: BACK_TILES[tile.to_i],
          logic:   get_logical(cur, tile),
        }
      end
    end
  end
end
