require "../utils/pool"
require "chipmunk"
require "./physics"

class PhysicsPool < Pool(Tuple(Game::MyBody, CP::Circle))
  def initialize(@space : CP::Space)
    super()
  end

  def reset
    raise "not implemented yet"
  end

  def get(&block)
    result = super { yield }
    @space.add(result[0])
    @space.add(result[1])
    result
  end

  def recycle(item)
    super
    @space.remove item[0]
    @space.remove item[1]
  end

  def get
    get do
      body = Game::MyBody.new
      shape = CP::Circle.new(body, 1.0)
      {body, shape}
    end
  end
end
