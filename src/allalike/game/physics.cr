require "chipmunk"
require "./general"

module Game
  class MyBody < CP::Body
    property owner : GameObject?
  end
end

macro define_collider(space, classa, classb)
  it = LibCP.space_add_collision_handler({{space}},
            CP::CollisionType.new({{classa}}.hash),
            CP::CollisionType.new({{classb}}.hash))
  it.value.pre_solve_func = ->(arb : LibCP::Arbiter*, aspace : LibCP::Space*, ptr : LibCP::DataPointer) do
    a = CP::Arbiter[arb].bodies[0].as(Game::MyBody).owner.as({{classa}})
    {% if classb.stringify == "Walls" %}
      a.collide_wall()
    {% else %}
      b = CP::Arbiter[arb].bodies[1].as(Game::MyBody).owner.as({{classb}})
      a.collide_{{classb.stringify.downcase.id}}(b)
    {% end %}
  end
end
