module Game
  abstract class Controlled < GameObject
    set_material(Flesh)
    include ThingsOwner
    property ammo : Int32
    getter scatter : Float64
    property target : CP::Vect
    property hp : Int32 = 1
    getter delay = 0
    getter maxdelay = 0

    def initialize(*args)
      super
      @ammo = 0
      @scatter = 0.0
      @control_body = CP::Body.new_kinematic
      @control_body.position = @body.position
      @target = CP.vzero
      @leash = CP::PivotJoint.new(@body, @control_body, CP.vzero, CP.vzero)
      @leash.max_bias = max_speed
      @leash.max_force = move_force
      @map.space.add(@leash)
    end

    def reset_weapon
      @ammo = weapon.maxammo
      @scatter = weapon.scatter0*2
    end

    def on_death
      @map.space.remove @leash
      super
    end

    def warp(x, y)
      super
      @target = CP.vzero
      @control_body.position = @body.position
    end

    def shoot(dx, dy)
      shoot Math.atan2(dy, dx)
    end

    abstract def shots_side : Int32
    abstract def side : Int32

    def shoot(angle)
      return if @delay > 0
      return if @ammo <= 0
      @maxdelay = @delay = weapon.shotspeed
      @ammo = @ammo - 1
      bullet_r = PHYS_DATA[BodyType::Bullet][:radius]
      weapon.burst.times do
        a = angle + 2*rand*@scatter - @scatter
        dx = Math.cos(a)
        dy = Math.sin(a)
        Bullet.new(@map, shots_side, weapon,
          x + dx*(radius + bullet_r + 1),
          y + dy*(radius + bullet_r + 1),
          vx + dx*BULLET_SPEED,
          vy + dy*BULLET_SPEED)
      end
      @scatter += weapon.scatterdelta
    end

    def reload
      return if @delay > 0
      return if @ammo >= weapon.maxammo
      @maxdelay = @delay = weapon.reloadspeed
      @ammo = (weapon.singlereload ? @ammo + 1 : weapon.maxammo)
    end

    def process
      @control_body.position = @body.position + @target
      @delay -= 1 if @delay > 0
      @scatter = weapon.scatter0 + (@scatter - weapon.scatter0)*0.95 if @scatter > weapon.scatter0
      die if @hp <= 0
    end
  end
end
