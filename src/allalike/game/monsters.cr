require "./general"
require "./shooter"
require "./players"
require "./loot"
require "./thing"

module Game
  class Monster < Controlled
    include ThingsOwner
    set_bodytype(Monster)
    property enemy : Player?

    def initialize(amap)
      super(amap, *amap.spawn_pos)
      @hp = 10
      item = Weapon.new(0)
      item.taken(self)
      item.wear
      @picture_id = 16*128 + rand(5)
      amap.nmonsters += 1
    end

    def on_death
      super
      @map.nmonsters -= 1
      # LootItem.new(@map, Thing.generate, x, y)
    end

    def retarget
      @enemy = @map.world.player
      if e = @enemy
        @enemy = nil if (e.pos - self.pos).lengthsq > 500*500
      end
    end

    def process
      enemy = @enemy
      retarget if rand < 0.01 || (enemy && enemy.dead) # || !enemy
      if enemy = @enemy
        self.target = enemy.pos - self.pos
        @angle = self.target.to_angle

        self.target = CP.vzero if self.target.lengthsq < weapon.maxrange*weapon.maxrange*0.9
        if @delay == 0
          if @ammo > 0
            delta = enemy.pos - self.pos
            delta += enemy.speed * (delta.length / BULLET_SPEED)
            shoot(delta.x, delta.y)
          else
            reload
          end
        end
      else
        reload
        self.target = CP.vzero
      end
      super
    end

    def shots_side : Int32
      0
    end

    def side : Int32
      0
    end

    def collide_player(player)
      # player.hp -= 1
      # player.die if player.hp <= 0
      true
    end
  end
end
