require "./general"

module Game
  class Map
    getter shapes_cache = {} of Tuple(Int32, Int32) => CP::Shape

    def can_spawn(x, y)
      # passable(x, y)
      cell(x, y)[:logic] == LogicalTile::Room
    end

    def passable(tile)
      BearLibMG::CellType.new(tile.to_i).passable?
    end

    def passable(x, y)
      return false unless (0..MAP_SIZE - 1).includes? x
      return false unless (0..MAP_SIZE - 1).includes? y
      map_data[y*MAP_SIZE + x][:pass]
    end

    def spawn_pos
      loop do
        x, y = (rand(MAP_SIZE - 2) + 1), (rand(MAP_SIZE - 2) + 1)
        return {x*MAP_TILE_SIZE + MAP_TILE_SIZE/2, y*MAP_TILE_SIZE + MAP_TILE_SIZE/2} if can_spawn(x, y)
      end
    end

    def drop_wall(x, y)
      @shapes_cache[{x, y}]?.try { |shape| space.remove(shape) }
    end
  end

  class Walls
    set_material Stone
    @shapes = [] of CP::Shape
    getter! map : Map

    private def fit_shape(shape)
      shape.friction = friction
      shape.elasticity = elasticity
      shape.collision_type = Walls.hash
      @shapes << shape
      map.space.add(shape)
    end

    BORDER = 5
    private def add_tile(x, y, scale)
      x1 = x*scale - BORDER
      y1 = y*scale - BORDER
      x2 = x1 + scale + BORDER
      y2 = y1 + scale + BORDER
      v = [CP.v(x1, y1), CP.v(x1, y2), CP.v(x2, y2), CP.v(x2, y1)]
      shape = CP::Poly.new(map.space.static_body, v)
      map.shapes_cache[{x, y}] = shape
      fit_shape(shape)
    end
    DOOR_WIDTH2 = 2
    private def add_door(x, y, vertical, scale)
      if vertical
        x1 = (x + 0.5)*scale - DOOR_WIDTH2
        y1 = y*scale - BORDER
        x2 = (x + 0.5)*scale + DOOR_WIDTH2
        y2 = y1 + scale + BORDER
      else
        x1 = x*scale - BORDER
        y1 = (y + 0.5)*scale - DOOR_WIDTH2
        x2 = x1 + scale + BORDER
        y2 = (y + 0.5)*scale + DOOR_WIDTH2
      end
      v = [CP.v(x1, y1), CP.v(x1, y2), CP.v(x2, y2), CP.v(x2, y1)]
      shape = CP::Poly.new(map.space.static_body, v)
      map.shapes_cache[{x, y}] = shape
      fit_shape(shape)
    end

    def generate_for_map(amap)
      @map = amap
      reset
      MAP_SIZE.times do |x|
        MAP_SIZE.times do |y|
          case amap.cell(x, y)[:logic]
          when LogicalTile::VertDoor
            add_door(x, y, true, MAP_TILE_SIZE)
          when LogicalTile::HorizDoor
            add_door(x, y, false, MAP_TILE_SIZE)
          when LogicalTile::Wall
            add_tile(x, y, MAP_TILE_SIZE)
          else
            #
          end
        end
      end
    end

    def reset
      @shapes.each do |shape|
        map.space.remove shape
      end
      @shapes.clear
    end
  end
end
