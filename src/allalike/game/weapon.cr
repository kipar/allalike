require "./weapon_data"
require "./thing"

private macro apply_base_params(*args)
  {% for name, index in args %}
    @{{name}} = WEAPON_DATA[@base_id][:{{name}}]
  {% end %}
end

module Game
  class Weapon < Thing
    getter! name : String
    getter! shotspeed : Int32
    getter! reloadspeed : Int32
    getter! maxammo : Int32
    getter! burst : Int32
    getter! scatter0 : Float64
    getter! scatterdelta : Float64
    getter! autofire : Bool
    getter! singlereload : Bool
    getter! damage : Int32
    getter! maxrange : Int32

    getter base_id
    getter bullet_type : PictureId

    #    ammoid, weight, chance, shottype, -- incomplete
    def initialize(@base_id : Int32)
      super()
      @bullet_type = PictureId.new(WEAPON_DATA[@base_id][:shot])
      apply_base_params(name,
        shotspeed, reloadspeed, scatter0, scatterdelta, maxammo, burst,
        #    ammoid, weight, chance, shottype,
        autofire, singlereload, damage, maxrange
      )
    end

    def picture_id
      PictureId.new(WEAPON_DATA[@base_id][:picture])
    end

    def desc
      self.name
    end

    def self.generate
      new(rand(WEAPON_DATA.size))
    end

    def slot
      Slot::Weapon
    end

    def wear
      super
      owner.not_nil!.reset_weapon
    end

    def takeoff
      super
      owner.not_nil!.reset_weapon
    end
  end

  class Ammo < Thing
    def initialize(@base_id : Int32)
      super()
    end

    def picture_id
      PictureId.new(@base_id + 101)
    end

    def desc : InvItem
      "TODO"
    end

    def self.generate
      new(rand(10))
    end

    def slot
      Slot::Ammo
    end

    def wear
      super
    end

    def takeoff
      super
    end
  end
end
