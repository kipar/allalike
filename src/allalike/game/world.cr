require "chipmunk"
require "chipmunk/lib"
require "../app"
require "../utils/counter"
require "./general"
require "./bullets"
require "./monsters"
require "./players"
require "./loot"
require "./map"
require "./walls"
require "./physics_pool"
require "./mapgen"
require "../bearlib/mapgen"

module Game
  class World
    getter player
    getter maps

    def initialize
      map = Map.new
      @maps = [] of Map
      @maps << map
      @player = Player.new(map, "test", 100)
      map.world = self
    end

    def process
      @maps.each &.process
      # TODO maps management
    end
  end
end
