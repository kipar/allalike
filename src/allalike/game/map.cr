require "chipmunk"
require "chipmunk/lib"
require "../app"
require "../utils/counter"
require "./general"
require "./players"
require "./walls"
require "./physics_pool"
require "./mapgen"
require "../bearlib/mapgen"

module Game
  class Map
    getter players = [] of Player
    getter space
    property! world : World
    getter physics_pool
    getter objects = [] of GameObject
    property nmonsters = 0
    getter map_data = Slice({pass: Bool, picture: PictureId, logic: LogicalTile}).new(MAP_SQRSIZE, {pass: false, picture: 0, logic: LogicalTile::Wall})

    def cell(x, y)
      @map_data[x + y*MAP_SIZE]
    end

    def initialize
      @space = CP::Space.new
      @space.damping = 1
      @space.gravity = CP.vzero
      @physics_pool = PhysicsPool.new(@space)
      # TODO - macros
      # register_collisions(Monster,Bullet,Walls,Player)
      define_collider(@space, Bullet, Monster)
      define_collider(@space, Bullet, Walls)
      define_collider(@space, Bullet, Player)
      define_collider(@space, Monster, Player)

      define_collider(@space, LootItem, Player)
      define_collider(@space, LootItem, Bullet)
      define_collider(@space, LootItem, Monster)

      # TestMapGenerator.new.generate @map_data
      # BearMapGenerator.new(typ: BearLibMG::MapGenerator::G_NICE_DUNGEON, double: true).generate @map_data
      BearMapGenerator.new(typ: BearLibMG::MapGenerator::G_NICE_DUNGEON, double: true).generate @map_data
      @walls = Walls.new
      @walls.generate_for_map self
    end

    def process
      @space.step(PHYS_DT)
      @objects.each do |obj|
        obj.process
      end
      @objects.reject! do |obj|
        if obj.dead
          obj.on_death
        end
        obj.dead
      end
      if @nmonsters < MAX_MONSTERS
        Monster.new(self)
      end
    end
  end
end
