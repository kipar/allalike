require "./general"
require "./shooter"

module Game
  class Player < Controlled
    set_bodytype Player
    getter name : String
    # TODO - change lists interface?
    property crunch = false

    def shoot(byteangle : UInt8)
      return if @dead
      angle = byte2angle(byteangle)
      shoot(angle)
    end

    def shots_side : Int32
      1
    end

    def side : Int32
      1
    end

    def reload
      return if @dead
      super
    end

    def open_door
      ax, ay = cellpos
      cell = @map.cell(ax, ay)
      return unless cell[:logic].door?
      @map.map_data[ax + ay*MAP_SIZE] = {
        pass:    true,
        picture: OPEN_DOORS[cell[:picture]],
        logic:   LogicalTile::Passage,
      }
      spawn {
        sleep 0.2
        @map.map_data[ax + ay*MAP_SIZE] = {
          pass:    true,
          picture: NO_DOORS[cell[:picture]],
          logic:   LogicalTile::Passage,
        }
        @map.drop_wall(ax, ay)
      }
    end

    def initialize(amap, @name, @hp)
      super(amap, *amap.spawn_pos)
      item = Weapon.new(0)
      item.taken(self)
      item.wear
      @picture_id = PictureId.new(3*128 + rand(12))
    end

    def on_death
      super
      # TODO - game over
    end
  end
end
