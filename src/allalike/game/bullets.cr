require "chipmunk"
require "./general"

module Game
  class Bullet < GameObject
    set_material(Shot)
    set_bodytype(Bullet)

    @ticks : Int32
    getter side
    getter weapon

    def initialize(amap, @side : Int32, @weapon : Weapon, ax, ay, avx, avy)
      super(amap, ax, ay)
      @ticks = @weapon.maxrange
      @body.velocity = CP.v(avx, avy)
      @picture_id = @weapon.bullet_type
    end

    def rico
      @ticks = 10 if @ticks > 10
    end

    def process
      @ticks -= 1
      die if @ticks <= 0
      super
    end

    def collide_player(player)
      return false if @side == player.side
      player.hp -= 1 # @weapon.damage/10
      die
      true
    end

    def collide_monster(monster)
      return false if @side == monster.side
      monster.hp -= @weapon.damage
      die
      true
    end

    def collide_wall
      rico
      true
    end
  end
end
