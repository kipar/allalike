require "chipmunk"
require "./physconfig"
require "./physics"
require "./weapon"

module Game
  MAP_SIZE      = 100
  MAP_SQRSIZE   = MAP_SIZE*MAP_SIZE
  PHYS_DT       = 1.0/60
  MAP_TILE_SIZE =  64
  MAX_MONSTERS  = 100

  alias PhysValue = Float64
  alias PictureId = Int32

  class GameObject
    property dead = false
    property angle = 0.0

    getter! picture_id : PictureId
    getter map : Map

    def warp(x, y)
      @body.position = CP.v(x, y)
    end

    def x
      @body.position.x
    end

    def y
      @body.position.y
    end

    def cellpos
      {(x/MAP_TILE_SIZE).to_i, (y/MAP_TILE_SIZE).to_i}
    end

    def pos
      @body.position
    end

    def vx
      @body.velocity.x
    end

    def vy
      @body.velocity.y
    end

    def speed
      @body.velocity
    end

    @body : MyBody
    @shape : CP::Circle

    def initialize(@map : Map, ax, ay)
      space = @map.space
      @body, @shape = @map.physics_pool.get
      @body.mass = mass
      @body.owner = self
      @body.moment = CP::Circle.moment(mass, 0.0, radius)
      @shape.radius = radius
      @shape.friction = friction
      @shape.elasticity = elasticity
      @shape.collision_type = self.class.hash
      @body.position = CP.v(ax, ay)
      @map.objects << self
    end

    def process
    end

    def on_death
      @map.space.remove @shape
      @map.space.remove @body
    end

    def die
      @dead = true
    end
  end

  BULLET_SPEED = 1000.0
end
