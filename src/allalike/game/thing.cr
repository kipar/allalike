module Game
  enum Slot
    Weapon
    Ammo
  end

  abstract class Thing
    getter owner : ThingsOwner?
    getter active = false

    abstract def slot : Slot
    abstract def picture_id : PictureId
    abstract def desc : String

    def taken(aowner)
      @owner = aowner
      aowner.inventory << self
    end

    def drop
      return unless aowner = @owner
      aowner.inventory.delete self
      @owner = nil
      # drop on map?
    end

    def use
      active ? takeoff : wear
    end

    def takeoff
      return unless aowner = @owner
      aowner.equip.delete slot
      @active = false
    end

    def wear
      return unless aowner = @owner
      aowner.equip[slot]?.try &.takeoff
      aowner.equip[slot] = self
      @active = true
    end

    def self.generate
      (rand < 0.3 ? Weapon : Ammo).generate
    end
  end

  module ThingsOwner
    getter inventory = [] of Thing
    getter equip = {} of Slot => Thing

    @melee = Weapon.new(0)

    def weapon : Weapon
      equip[Slot::Weapon]?.try(&.as(Weapon)) || @melee
    end
  end
end
