require "chipmunk"

module Game
  enum Material
    Flesh
    Stone
    Shot
  end

  enum BodyType
    Player
    Monster
    Bullet
  end

  MAT_DATA = {
    Material::Flesh => {friction: 0.1, elasticity: 0.1},
    Material::Stone => {friction: 0.5, elasticity: 1.0},
    Material::Shot  => {friction: 0.1, elasticity: 0.1},
  }

  PHYS_DATA = {
    BodyType::Player  => {mass: 2.0, radius: 16.0, max_speed: 400.0, move_force: 10000.0},
    BodyType::Monster => {mass: 2.0, radius: 16.0, max_speed: 400.0, move_force: 1000.0},
    BodyType::Bullet  => {mass: 0.002, radius: 2.0, max_speed: 400.0, move_force: 1000.0},
  }
end

macro set_material(mat)
{% for name, index in [:friction, :elasticity] %}
  def {{name.id}} : PhysValue
    MAT_DATA[Material::{{mat}}][{{name}}]
  end
{% end %}
end

macro set_bodytype(typ)
{% for name, index in [:mass, :radius, :max_speed, :move_force] %}
  def {{name.id}} : PhysValue
    PHYS_DATA[BodyType::{{typ}}][{{name}}]
  end
{% end %}
end
