require "./config"

module Allalike
  enum InvAction
    Drop
    Use
    Check
  end

  class Inventory
    getter active
    @selected : Int32?
    @n_x : Int32
    @n_y : Int32

    def initialize(ax, ay, aw, ah)
      @base_x = Engine::UICoord.new(ax * Engine::SCREENX)
      @base_y = Engine::UICoord.new(ay * Engine::SCREENY)
      @slot_w = 50
      @slot_h = 50
      @n_x = (aw*Engine::SCREENX / (@slot_w + 1)).to_i
      @n_y = (ah*Engine::SCREENY / (@slot_h + 1)).to_i
      @width = Engine::UICoord.new(@n_x * (@slot_w + 1))
      @height = Engine::UICoord.new(@n_y * (@slot_h + 1))

      @active = false
      # @items = []
      @desc = ""
    end

    private def coord2index(x, y)
      return nil unless @base_x < x || x < @base_x + @width
      return nil unless @base_y < y || y < @base_y + @height
      ((x - @base_x).to_i / (@slot_w + 1)) + ((y - @base_y).to_i / (@slot_h + 1))*@n_x
    end

    def update_list(index : Int32? = nil)
      @selected = nil
      # @items = @network.actions.inv_getlist
      @desc = ""
    end

    def item_action(x, y, action)
      index = coord2index(x, y)
      return false unless index
      # case action
      # when .check?
      #   @selected = index
      #   @desc = @network.actions.inv_getdesc(index) || ""
      # when .drop?
      #   @network.actions.inv_drop(index)
      #   update_list index
      # when .use?
      #   @network.actions.inv_use(index)
      #   update_list index
      # else
      #   raise "incorrect InvAction"
      # end
      true
    end

    def show
      update_list
      @active = true
    end

    def hide
      @active = false
      @selected = nil
      # @items.clear
    end

    PIC_SIDE = 16

    def draw(immediate)
      return unless @active
      bgcolor = SF::Color.new(0, 0, 0, 100)
      activecolor = SF::Color::Green
      text_color = SF::Color::White

      index = 0
      @n_y.times do |y|
        @n_x.times do |x|
          # it = @items[index]?
          # highlighted = it && it[1]
          # xpos = @base_x + x*(@slot_w + 1)
          # ypos = @base_y + y*(@slot_h + 1)
          # immediate.rect(xpos, ypos, @slot_w, @slot_h, highlighted ? activecolor : bgcolor)
          # if it
          #   scale = 1.0
          #   tex = it[0] < 100 ? LOOT_BASE + it[0] : LOOT_BASE + it[0] - 100 + PIC_SIDE*2
          #   immediate.tile(xpos, ypos, tex, width: 1.0*@slot_w / immediate.tile_size, height: 1.0*@slot_h / immediate.tile_size)
          # end
          index += 1
        end
      end
    end
  end
end
