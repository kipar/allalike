@[Link("bearlibmg")]
lib BearLibMG
  alias TGeneratorParamsHandle = Void*
  alias TRoomsDataHandle = Void*
  alias TCoord = Int32

  enum MapGenerator
    G_ANT_NEST       =  1,
    G_CAVES          =  2,
    G_VILLAGE        =  3,
    G_LAKES          =  4,
    G_LAKES2         =  5,
    G_TOWER          =  6,
    G_HIVE           =  7,
    G_CITY           =  8,
    G_MOUNTAIN       =  9,
    G_FOREST         = 10,
    G_SWAMP          = 11,
    G_RIFT           = 12,
    G_TUNDRA         = 13,
    G_BROKEN_CITY    = 14,
    G_BROKEN_VILLAGE = 15,
    G_MAZE           = 16,
    G_CASTLE         = 17,
    G_WILDERNESS     = 18,
    G_NICE_DUNGEON   = 19
  end

  enum CellType
    TILE_CAVE_WALL   =  0,
    TILE_GROUND      =  1,
    TILE_WATER       =  2,
    TILE_TREE        =  3,
    TILE_MOUNTAIN    =  4,
    TILE_ROAD        =  6,
    TILE_HOUSE_WALL  =  7,
    TILE_HOUSE_FLOOR =  8,
    TILE_GRASS       =  9,
    TILE_EMPTY       = 10,

    TILE_CORRIDOR  = 11,
    TILE_SMALLROOM = 12,
    TILE_BIGROOM   = 13,
    TILE_ROOMWALL  = 14,
    TILE_DOOR      = 15
  end

  alias TMapCallback = (TCoord, TCoord, CellType, Void* -> Void)

  fun generate = mg_generate(map_id : Int32, layer : Int32, typ : MapGenerator, seed : UInt32, params : TGeneratorParamsHandle, rooms_data : TRoomsDataHandle)
  fun generate_cb = mg_generate_cb(size_x : TCoord, size_y : TCoord, typ : MapGenerator, seed : UInt32, callback : TMapCallback, opaque : Void*, params : TGeneratorParamsHandle, rooms_data : TRoomsDataHandle)
  fun params_create = mg_params_create(typ : MapGenerator) : TGeneratorParamsHandle
  fun params_delete = mg_params_delete(params : TGeneratorParamsHandle)
  fun params_set = mg_params_set(params : TGeneratorParamsHandle, param : LibC::Char*, value : Int32)
  fun params_setf = mg_params_setf(params : TGeneratorParamsHandle, param : LibC::Char*, value : LibC::Float)
  fun params_get = mg_params_get(params : TGeneratorParamsHandle, param : LibC::Char*) : Int32
  fun params_getf = mg_params_getf(params : TGeneratorParamsHandle, param : LibC::Char*) : LibC::Float
  fun params_setstr = mg_params_setstr(params : TGeneratorParamsHandle, param : LibC::Char*, value : LibC::Char*) : LibC::Float
  fun roomsdata_create = mg_roomsdata_create(typ : MapGenerator) : TRoomsDataHandle
  fun roomsdata_delete = mg_roomsdata_delete(rooms : TRoomsDataHandle)
  fun roomsdata_count = mg_roomsdata_count(rooms : TRoomsDataHandle) : Int32
  fun roomsdata_position = mg_roomsdata_position(rooms : TRoomsDataHandle, room_index : Int32, ax0 : TCoord*, ay0 : TCoord*, awidth : TCoord*, aheight : TCoord*)
  fun roomsdata_linkscount = mg_roomsdata_linkscount(rooms : TRoomsDataHandle, room_index : Int32) : Int32
  fun roomsdata_getlink = mg_roomsdata_getlink(rooms : TRoomsDataHandle, room_index : Int32, link_index : Int32) : Int32
end
