@[Link("bearlibpf")]
lib BearLibPF
  fun create = pf_create(map_id : Int32T, layer : Int32T, algorithm : TpfAlgorithm, diagonal_cost : LibC::Float) : TPathfinderHandle
  alias Int32T = LibC::Int
  alias Uint32T = LibC::UInt
  alias TpfAlgorithm = Uint32T
  alias TPathfinderHandle = Void*
  fun create_cb = pf_create_cb(size_x : TCoord, size_y : TCoord, algorithm : TpfAlgorithm, diagonal_cost : LibC::Float, opaque : Void*, callback : TpfCallback) : TPathfinderHandle
  alias TCoord = Int32T
  alias TpfCallback = (TCoord, TCoord, TCoord, TCoord, Void* -> LibC::Float)
  fun delete = pf_delete(pathfinder : TPathfinderHandle)
  fun set_opaque = pf_set_opaque(pathfinder : TPathfinderHandle, opaque : Void*)
  fun calculate_path = pf_calculate_path(pathfinder : TPathfinderHandle, fromx : TCoord, fromy : TCoord, tox : TCoord, toy : TCoord) : TPathHandle
  alias TPathHandle = Void*
  fun calculate_path_gc = pf_calculate_path_gc(pathfinder : TPathfinderHandle, key : Void*, fromx : TCoord, fromy : TCoord, tox : TCoord, toy : TCoord) : TPathHandle
  fun path_origin = pf_path_origin(path : TPathHandle, fromx : TCoord, fromy : TCoord)
  fun path_destination = pf_path_destination(path : TPathHandle, tox : TCoord, toy : TCoord)
  fun path_empty = pf_path_empty : LibC::Int
  fun path_revert = pf_path_revert(path : TPathHandle)
  fun path_length = pf_path_length(path : TPathHandle) : Int32T
  fun path_distance = pf_path_distance(path : TPathHandle) : LibC::Float
  fun path_get = pf_path_get(path : TPathHandle, index : Int32T, x : TCoord*, y : TCoord*)
  fun path_walk = pf_path_walk : LibC::Int
  fun path_delete = pf_path_delete(path : TPathHandle)
  fun dijkstra_distance = pf_dijkstra_distance(pathfinder : TPathfinderHandle, x : TCoord, y : TCoord) : LibC::Float
  fun dijkstra_length = pf_dijkstra_length(pathfinder : TPathfinderHandle, x : TCoord, y : TCoord) : Int32T
  fun dijkstra_calculate = pf_dijkstra_calculate(pathfinder : TPathfinderHandle, tox : TCoord, toy : TCoord)
  fun dijkstra_find_unreachable = pf_dijkstra_find_unreachable : LibC::Int
end

